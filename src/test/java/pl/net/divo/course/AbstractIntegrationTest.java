package pl.net.divo.course;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AbstractIntegrationTest {
    @LocalServerPort

    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();

    <T> T doGet(String uri, Class<T> tClass) {
        return restTemplate.getForEntity(
                createURLWithPort(uri),
                tClass
        ).getBody();
    }

    <T> T doGet(String uri, TypeReference<T> tClass) {
        ResponseEntity<String> response = restTemplate.getForEntity(
                createURLWithPort(uri),
                String.class
        );

        ObjectMapper om = new ObjectMapper();
        return Optional.ofNullable(response.getBody()).map(t -> {
            try {
                return om.readValue(t, tClass);
            } catch (JsonProcessingException e) {
                throw new IllegalArgumentException(e);
            }
        }).orElse(null);
    }

    void doDelete(String uri) {
        restTemplate.delete(createURLWithPort(uri));
    }

    <T> void doPost(String uri, Object entity) {
        restTemplate.postForEntity(
                createURLWithPort(uri),
                entity,
                String.class
        );
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
