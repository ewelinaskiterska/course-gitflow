package pl.net.divo.course;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.net.divo.course.model.User;
//import pl.net.divo.course.repository.UserRepository;

import java.util.List;

class UserControllerTest extends AbstractIntegrationTest {
//    @Autowired
//    UserRepository userRepository;
//
//    @Test
//    void shouldNotFindAnyUsers() {
//        // when
//        List<User> users = doGet("/users", new TypeReference<List<User>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, users.size());
//    }
//
//    @Test
//    void shouldAddTestWithoutErrors() {
//        // given
//        User user = new User("1", "name", "tester");
//
//        // when
//        doPost("/users", user);
//
//        // then
//        Assertions.assertEquals(1, userRepository.findAll().size());
//        Assertions.assertEquals("name", userRepository.findById("1").getName());
//        Assertions.assertEquals("tester", userRepository.findById("1").getTester());
//    }
//
//    @Test
//    void shouldFindAllUsers() {
//        // given
//        doPost("/users", new User("1", "name1", "tester1"));
//        doPost("/users", new User("2", "name2", "tester2"));
//
//        // when
//        List<User> users = doGet("/users", new TypeReference<List<User>>() {
//        });
//
//        // then
//        Assertions.assertEquals(2, users.size());
//        Assertions.assertEquals("name1", users.get(0).getName());
//        Assertions.assertEquals("name2", users.get(1).getName());
//    }
//
//    @Test
//    void shouldFindById() {
//        // given
//        doPost("/users", new User("1", "name1", "tester1"));
//
//        // when
//        User user = doGet("/users/1", User.class);
//
//        // then
//        Assertions.assertEquals("1", user.getId());
//        Assertions.assertEquals("name1", user.getName());
//        Assertions.assertEquals("tester1", user.getTester());
//    }
//
//    @Test
//    void shouldFindByName() {
//        // given
//        doPost("/users", new User("1", "name1", "tester1"));
//
//        // when
//        List<User> user = doGet("/users?byName=name1", new TypeReference<List<User>>() {
//        });
//
//        // then
//        Assertions.assertEquals(1, user.size());
//        Assertions.assertEquals("1", user.get(0).getId());
//        Assertions.assertEquals("name1", user.get(0).getName());
//        Assertions.assertEquals("tester1", user.get(0).getTester());
//    }
//
//    @Test
//    void shouldNotFindByNameWhenNotExist() {
//        // given
//        doPost("/users", new User("1", "name1", "tester1"));
//
//        // when
//        List<User> user = doGet("/users?byName=name2", new TypeReference<List<User>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, user.size());
//    }
//
//    @Test
//    void shouldDeleteUser() {
//        // given
//        doPost("/users", new User("1", "name1", "tester1"));
//        List<User> originalUsers = userRepository.findAll();
//
//        // when
//        doDelete("/users/1");
//        List<User> userAfterDelete = userRepository.findAll();
//
//        // then
//        Assertions.assertEquals(1, originalUsers.size());
//        Assertions.assertEquals("name1", originalUsers.get(0).getName());
//        Assertions.assertEquals(0, userAfterDelete.size());
//    }
//
//    @AfterEach
//    void clean() {
//        userRepository.deleteAll();
//    }
}
