package pl.net.divo.course;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.net.divo.course.model.Car;
//import pl.net.divo.course.repository.CarRepository;

import java.util.List;

class CarControllerTest extends AbstractIntegrationTest {
//    @Autowired
//    CarRepository carRepository;
//
//    @Test
//    void shouldNotFindAnyCars() {
//        // when
//        List<Car> cars = doGet("/tests", new TypeReference<List<Car>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, cars.size());
//    }
//
//    @Test
//    void shouldAddTestWithoutErrors() {
//        // given
//        Car car = new Car("1", "name", "tester");
//
//        // when
//        doPost("/tests", car);
//
//        // then
//        Assertions.assertEquals(1, carRepository.findAll().size());
//        Assertions.assertEquals("name", carRepository.findById("1").getName());
//        Assertions.assertEquals("tester", carRepository.findById("1").getTester());
//    }
//
//    @Test
//    void shouldFindAllCars() {
//        // given
//        doPost("/tests", new Car("1", "name1", "tester1"));
//        doPost("/tests", new Car("2", "name2", "tester2"));
//
//        // when
//        List<Car> cars = doGet("/tests", new TypeReference<List<Car>>() {
//        });
//
//        // then
//        Assertions.assertEquals(2, cars.size());
//        Assertions.assertEquals("name1", cars.get(0).getName());
//        Assertions.assertEquals("name2", cars.get(1).getName());
//    }
//
//    @Test
//    void shouldFindById() {
//        // given
//        doPost("/tests", new Car("1", "name1", "tester1"));
//
//        // when
//        Car car = doGet("/tests/1", Car.class);
//
//        // then
//        Assertions.assertEquals("1", car.getId());
//        Assertions.assertEquals("name1", car.getName());
//        Assertions.assertEquals("tester1", car.getTester());
//    }
//
//    @Test
//    void shouldFindByName() {
//        // given
//        doPost("/tests", new Car("1", "name1", "tester1"));
//
//        // when
//        List<Car> car = doGet("/tests?byName=name1", new TypeReference<List<Car>>() {
//        });
//
//        // then
//        Assertions.assertEquals(1, car.size());
//        Assertions.assertEquals("1", car.get(0).getId());
//        Assertions.assertEquals("name1", car.get(0).getName());
//        Assertions.assertEquals("tester1", car.get(0).getTester());
//    }
//
//    @Test
//    void shouldNotFindByNameWhenNotExist() {
//        // given
//        doPost("/tests", new Car("1", "name1", "tester1"));
//
//        // when
//        List<Car> car = doGet("/tests?byName=name2", new TypeReference<List<Car>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, car.size());
//    }
//
//    @Test
//    void shouldDeleteCar() {
//        // given
//        doPost("/tests", new Car("1", "name1", "tester1"));
//        List<Car> originalCars = carRepository.findAll();
//
//        // when
//        doDelete("/tests/1");
//        List<Car> carAfterDelete = carRepository.findAll();
//
//        // then
//        Assertions.assertEquals(1, originalCars.size());
//        Assertions.assertEquals("name1", originalCars.get(0).getName());
//        Assertions.assertEquals(0, carAfterDelete.size());
//    }
//
//    @AfterEach
//    void clean() {
//        carRepository.deleteAll();
//    }
}
