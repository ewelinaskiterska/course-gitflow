package pl.net.divo.course.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.net.divo.course.model.Sample;
import pl.net.divo.course.repository.SampleRepository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@AllArgsConstructor
public class SampleController {
    private final SampleRepository sampleRepository;

    @GetMapping("/tests/isHealthy")
    public String isHealthy() {
        return "OK";
    }

    @GetMapping("/tests")
    public List<Sample> list(@RequestParam(required = false, name = "byName") String name) {
        if (name == null) {
            return sampleRepository.findAll();
        }

        return sampleRepository.findByName(name).map(s -> Stream.of(s).collect(Collectors.toList())).orElse(Collections.emptyList());
    }

    @DeleteMapping("/tests/{id}")
    public void remove(@PathVariable String id) {
        sampleRepository.delete(id);
    }

    @PostMapping("/tests")
    public void add(@RequestBody Sample sample) {
        sampleRepository.save(sample);
    }

    @GetMapping("/tests/{id}")
    public Sample getById(@PathVariable String id) {
        return sampleRepository.findById(id);
    }
}
