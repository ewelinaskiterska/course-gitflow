package pl.net.divo.course.model;

public class Cat {
    private final String id;
    private final String name;
    private final String owner;

    public Cat(String id, String name, String owner) {
        this.id = id;
        this.name = name;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }
}
